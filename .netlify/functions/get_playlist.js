const Airtable = require('airtable');
const { AIRTABLE_API } = process.env;
const { AIRTABLE_BASE } = process.env;
Airtable.configure({
    endpointUrl: "https://api.airtable.com",
    apiKey: AIRTABLE_API
})
const base = Airtable.base(AIRTABLE_BASE);

exports.handler = async() => {
    try {
        // Get records, sort (by default) by date added (subbed)
        const resp = await base("Videos").select({
            fields: ["id","title","date_uploaded","description","date_modified","date_subbed","subfile", "dllink","series"],
            filterByFormula: "{live}=TRUE()", 
            sort:[{field: "date_subbed", direction: "desc"}]
        }).firstPage()

        // Extract just the fields from response
        let fields = [];
        resp.forEach(record => fields.push(record.fields));

        // Return the response fields as JSON
        return {
            statusCode: 200,
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(fields)
        }

    }
    catch(err) {
        return{
            statusCode: 400,
            body: JSON.stringify({message: err.message})
        }
    }
}